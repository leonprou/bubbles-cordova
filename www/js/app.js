// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('bubbles',
  [
    'issue-9128-patch',
    'ionic',
    'satellizer',
    'ngFileUpload',
    'app.common.config',
    'app.common.controllers',
    'app.common.directives',
    'app.common.services',
    'app.chat.controllers',
    'app.chat.services',
    'app.explore.controllers',
    'app.explore.services',
    'app.profile.controllers',
    'app.auth.controllers',
    'app.request.controllers',
    'app.request.services'
  ])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  function authenticate($q, $location, $auth) {
    var deferred = $q.defer();

    if (!$auth.isAuthenticated()) {
      $location.path('/auth/login');
    } else {
      deferred.resolve();
    }

    return deferred.promise;
  }

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    resolve: {
      authenticated: authenticate
    }
  })

  .state('app.chat', {
    url: '/chat',
    abstract: true,
    views: {
      'tab-chats': {
        template: '<ion-nav-view></ion-nav-view>'
      }
    }
  })

  .state('app.chat.list', {
    url: '/list',
    templateUrl: 'templates/chat/list.html',
    controller: 'ChatListCtrl'
  })

  .state('app.chat.view', {
    url: '/view/:chatId/:receiverId',
    templateUrl: 'templates/chat/view.html',
    controller: 'ChatCtrl'
  })

  .state('app.explore', {
    url: '/explore',
    abstract: true,
    views: {
      'tab-explore': {
        template: '<ion-nav-view></ion-nav-view>'
      }
    }
  })

  .state('app.explore.view', {
    url: '/view',
    templateUrl: 'templates/explore/view.html',
    controller: 'ExploreViewCtrl'
  })

  .state('app.explore.my-topics', {
    url: '/view',
    templateUrl: 'templates/common/my-topics.html',
    controller: 'MyTopicsCtrl'
  })

  .state('app.request', {
    url: '/request',
    abstract: true,
    views: {
      'tab-request': {
        template: '<ion-nav-view></ion-nav-view>'
      }
    }
  })

  .state('app.request.list', {
    url: '/list',
    templateUrl: 'templates/request/list.html',
    controller: 'RequestsListCtrl'
  })

  .state('app.profile', {
    url: '/profile',
    abstract: true,
    views: {
      'tab-profile': {
        template: '<ion-nav-view></ion-nav-view>'
      }
    }
  })

  .state('app.profile.view', {
    url: '/account',
    templateUrl: 'templates/profile/view.html',
    controller: 'ProfileCtrl'
  })

  .state('app.profile.picture', {
    url: '/picture/:url',
    templateUrl: 'templates/profile/picture.html',
    controller: 'PictureCtrl'
  })

  .state('auth', {
    url: '/auth',
    abstract: true,
    template: '<ion-nav-view></ion-nav-view>'
  })

  .state('auth.login', {
    url: '/login',
    templateUrl: 'templates/auth/login.html',
    controller: 'LoginCtrl'
  })

  .state('auth.register', {
    url: '/register',
    templateUrl: 'templates/auth/register.html',
    controller: 'RegisterCtrl'
  });

  $urlRouterProvider.otherwise('/app/chat/list');

})

.config(function($authProvider, BaseUrl, Auth) {
  $authProvider.baseUrl = BaseUrl;
  // Configuration common for all providers.
  var commonConfig = {
    // Popup should expand to full screen with no location bar/toolbar.
    popupOptions: {
      location: 'no',
      toolbar: 'no',
      width: window.screen.width,
      height: window.screen.height
    }
  };

  // Change the platform and redirectUri only if we're on mobile
  // so that development on browser can still work.
  if (ionic.Platform.isIOS() || ionic.Platform.isAndroid()) {
    $authProvider.platform = 'mobile';
    commonConfig.redirectUri = 'http://localhost/';
  }

  // Configure Facebook login.
  $authProvider.facebook(angular.extend({}, commonConfig, {
    clientId: Auth.facebookAppId,
    url: '/auth/facebook'
  }));

})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.views.transition('none');
  $ionicConfigProvider.tabs.position('bottom');
});
