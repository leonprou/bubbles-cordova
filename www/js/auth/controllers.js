angular.module('app.auth.controllers', ['satellizer', 'ngStorage'])

.controller('LoginCtrl', function($scope, $auth, $state, $ionicPopup,
    $ionicHistory, $localStorage, UserSrvc) {
  $scope.user = $localStorage.user;

  $scope.login = function(user) {
    UserSrvc.login(user)
      .then(function(response) {
        user.password = null;
        if ($localStorage.user) {
          $localStorage.user.email = user.email;
        } else {
          $localStorage.user = {
            email: user.email
          };
        }
        $ionicHistory.nextViewOptions({
          historyRoot: true
        });
        $state.go('app.chat.list');
      })
      .catch(function(response) {
        $ionicPopup.alert({
          title: 'Error Occurred',
          template: response.data && response.data.message
        });
      });
  };

  $scope.authenticate = function(provider) {
    $auth.authenticate(provider);
  };
})

.controller('RegisterCtrl', function($scope, $auth, $state,
    $ionicPopup, $localStorage, $ionicHistory, UserSrvc) {
  $scope.signup = function(user) {
    // $auth.signup({
    //   username: user.username,
    //   email: user.email,
    //   password: user.password
    // })
    UserSrvc.signup(user).then(function(respone) {
      user.password = null;
      if ($localStorage.user) {
        $localStorage.user.email = user.email;
      } else {
        $localStorage.user = {
          email: user.email
        };
      }
      $ionicHistory.nextViewOptions({
        historyRoot: true
      });
      $state.go('app.chat.list');
    })
    .catch(function(response) {
      $ionicPopup.alert({
        title: 'Error Occurred',
        template: response.data && response.data.message
      });
    });
  };
});
