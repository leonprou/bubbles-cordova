angular.module('app.chat.controllers', [])

.controller('ChatListCtrl', function($scope, $state, $ionicHistory, ChatSrvc,
    MessageSrvc, DefaultPicture) {
  var page = 0;

  function checkChats() {
    if ($scope.chats.length === 0) {
      $scope.noChats = true;
    } else {
      $scope.noChats = false;
    }
  }

  $scope.chats = [];
  $scope.defaultPicture = DefaultPicture;

  $scope.$on('$ionicView.enter', function() {
    page = 0;
    $scope.moreDataCanBeLoaded = true;
    $scope.loadMore();
  });

  $scope.$on('$ionicView.loaded', function() {
    MessageSrvc.receiveMessage()
      .then(angular.noop, angular.noop, function(msg) {
        console.log(msg);
      });
  });

  $scope.$on('$stateChangeStart', function() {
    $scope.chats = [];
  });


  $scope.delete = function(chat) {
    ChatSrvc.delete(_.pick(chat, 'id', 'userId', 'topics'));
    $scope.chats.splice(_.indexOf($scope.chats, chat), 1);
    checkChats();
  };

  $scope.loadMore = function() {
    ChatSrvc.get(page).then(function(chats) {
      if (!chats || chats.length === 0) {
        $scope.moreDataCanBeLoaded = false;
      }
      $scope.chats = $scope.chats.concat(chats);
      checkChats();
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
    page++;
  };
})

.controller('ChatCtrl', function($scope, $stateParams, $auth, $ionicScrollDelegate,
    $timeout, MessageSrvc, UserSrvc) {

  var receiverId = Number($stateParams.receiverId);
  var chatId = Number($stateParams.chatId);
  var page = 0;

  MessageSrvc.getMessages(chatId, page).then(function(messages) {
    if (messages) {
      $scope.messages = messages.reverse();
      $scope.moreMessages = messages.length !== 0 ? true : false;
      $ionicScrollDelegate.scrollBottom();
    } else {
      $scope.messages = [];
    }
  });

  MessageSrvc.receiveMessage()
    .then(angular.noop, angular.noop, function(msg) {
      $scope.messages.push(msg);
      $ionicScrollDelegate.scrollBottom();
    });

  $scope.myId = UserSrvc.getId();
  $scope.hideTime = true;

  $scope.sendMessage = function() {
    MessageSrvc.sendMessage(chatId, receiverId, $scope.message.text);
    $scope.messages.push({
      text: $scope.message.text,
      senderId: $scope.myId,
      receiverId: receiverId
    });
    $scope.message.text = null;
  };

  $scope.loadMore = function() {
    MessageSrvc.getMessages(chatId, page).then(function(messages) {
      $scope.messages = messages.reverse().concat($scope.messages);
      $scope.moreMessages = messages.length !== 0 ? true : false;
    });
    page++;
  };
});
