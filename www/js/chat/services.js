/*globals io*/

angular.module('app.chat.services', [])

.factory('ChatSrvc', function($http, BaseApiUrl) {
  return {
    get: function(page) {
      return $http.get(BaseApiUrl + '/chat/' + page)
        .then(function(result) {
          return result.data;
        });
    },
    delete: function(chat) {
      return $http.delete(BaseApiUrl + '/chat/' + chat.id)
        .then(function(result) {
          return result.data;
        });
    }
    // add: function(topics) {
    //   return $http.post(BaseApiUrl + '/chat', {
    //     topics: _.pluck(topics, 'name')
    //   })
    //     .then(function(result) {
    //       return result.data;
    //     });
    // }
  };
})

.factory('MessageSrvc', function($http, $auth, $rootScope, $q, BaseUrl, BaseApiUrl) {
  var socket = io.connect(BaseUrl);
  return {
    getMessages: function(chatId, page) {
      return $http.get(BaseApiUrl + '/message/' + chatId + '/' + page)
        .then(function(result) {
          return result.data;
        });
    },
    receiveMessage: function (callback, $scope) {
      var deferred = $q.defer();
      socket.on('chat message:' + $auth.getPayload().sub, function (msg) {
        deferred.notify(msg);
      });

      return deferred.promise;
    },
    sendMessage: function(chatId, receiverId, text) {
      socket.emit('chat message', {
        chatId: chatId,
        receiverId: receiverId,
        text: text,
        token: $auth.getToken()
      });
    }
  };
});
