angular.module('app.common.controllers', [])

.controller('TopicBoxCtrl', function($scope, TopicSrvc) {
  $scope.holdTopic = function(event) {
    event.target.classList.add('selected');
  };

  $scope.releaseTopic = function(event) {
    event.target.classList.remove('selected');
  };

  $scope.selectTopic = function(topic) {
    var index = $scope.topics.indexOf(topic);
    $scope.topics.splice(index, 1);
    if (!_.find($scope.selectedTopics, { name: topic.name})) {
      $scope.selectedTopics.push(topic);
      $scope.$emit('topic.selected', topic);
    }
  };

  $scope.removeTopic = function(topic) {
    var index = $scope.selectedTopics.indexOf(topic);
    $scope.selectedTopics.splice(index, 1);
    $scope.topics.push(topic);
    $scope.$emit('topic.removed', topic);
  };

  $scope.addTopic = function() {
    if ($scope.topic.name) {
      $scope.selectedTopics.push($scope.topic);
      $scope.$emit('topic.selected', $scope.topic);
      $scope.topic.text = null;
    }
  };
})

.controller('MyTopicsCtrl', function($scope, $auth, $ionicHistory, $state, TopicSrvc) {

  function getTopics() {
    TopicSrvc.get($scope.search.text)
      .then(function(topics) {
        $scope.topics = topics;
      });
  }

  window.addEventListener('native.keyboardshow', function(event) {
    angular.element(document.getElementById('profileContent'))
      .css('position', 'fixed')
      .css('top', event.keyboardHeight + 'px');
  });

  window.addEventListener('native.keyboardhide', function(event) {
    angular.element(document.getElementById('profileContent'))
      .removeAttr('style');
  });

  $scope.search = {};

  $scope.$on('$ionicView.enter', getTopics);

  $scope.$on('$ionicView.enter', function() {
    TopicSrvc.getMine()
      .then(function(topics) {
        $scope.selectedTopics = topics.map(function(topic) {
          return { name: topic };
        });
      });
  });

  $scope.$on('$stateChangeStart', function() {
    $scope.topics = [];
    $scope.selectedTopics = [];
  });

  $scope.$on('topic.selected', function(event, topic) {
    TopicSrvc.add(topic.name);
    event.stopPropagation();
  });

  $scope.$on('topic.removed', function(event, topic) {
    TopicSrvc.remove(topic.name);
    event.stopPropagation();
  });

  $scope.addTopic = function() {
    TopicSrvc.add($scope.search.text);
    $scope.selectedTopics.push({
      name: $scope.search.text
    });
    $scope.search.text = null;
    getTopics();
  };

  $scope.getTopics = getTopics;
})

;
