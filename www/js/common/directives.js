angular.module('app.common.directives', [])

.directive('topicBox', function() {
  return {
    restrict: 'E',
    controller: 'TopicBoxCtrl',
    templateUrl: 'templates/common/topic-box.html'
  };
})

.directive('hideTabs', function($rootScope) {
  return {
    restrict: 'A',
    link: function($scope, $el) {
      $rootScope.hideTabs = true;
      $scope.$on('$destroy', function() {
        $rootScope.hideTabs = false;
      });
    }
  };
});
