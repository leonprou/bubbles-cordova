angular.module('app.common.services', [])

.factory('UserSrvc', function($auth, $http, $q, BaseApiUrl, DefaultPicture) {
  var me;

  return {
    getId: function() {
      return $auth.getPayload().sub;
    },
    login: function(user) {
      return $auth.login({
        email: user.email,
        password: user.password
      }).then(function(result) {
        me = result.data.user;
      });
    },
    signup: function(user) {
      return $auth.signup({
            username: user.username,
            email: user.email,
            password: user.password
      }).then(function(result) {
        me = result.data.user;
      });
    },
    authenticate: function(provider) {
      return $auth.authenticate(provider)
        .then(function(result) {
          me = result.data.user;
        });
    },
    me: function(update) {
      if (update || !me) {
        return $http.get(BaseApiUrl + '/me')
          .then(function(result) {
            me = result.data;
            return _.clone(me);
          });
      } else {
        return $q.when(_.clone(me));
      }
    },
    update: function(user) {
      var difference = _.omit(user, function(value, key) {
        return me[key] === value;
      });
      if (difference) {
        return $http.put(BaseApiUrl + '/me', difference)
          .then(function(result) {
            _.assign(me, difference);
            return _.clone(me);
          })
          .catch(function(response) {
            throw response.data.message;
          });
      }
    }
  };
})

.factory('ImageUploader', function($http, BaseApiUrl, Upload) {
  return {
    upload: function(file) {
      Upload.upload({
        url: BaseApiUrl + '/picture/upload',
        file: file
      });
    }
  };
})

.constant('DefaultPicture', 'img/default-picture.png');
