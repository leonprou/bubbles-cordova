angular.module('app.explore.controllers', [])

.controller('ExploreViewCtrl', function($scope, $state, $http, $ionicLoading,
    TopicSrvc, RequestSrvc) {

  function getTopics() {
    TopicSrvc.get($scope.search.text)
      .then(function(topics) {
        $scope.topics = topics;
      });
  }

  $scope.search = {};
  $scope.selectedTopics = [];

  $scope.$on('$ionicView.enter', getTopics);

  $scope.$on('$stateChangeStart', function() {
    $scope.topics = [];
  });

  window.addEventListener('native.keyboardshow', function(event) {
    angular.element(document.getElementById('exploreContent'))
      .css('position', 'fixed')
      .css('top', event.keyboardHeight + 'px');
  });

  window.addEventListener('native.keyboardhide', function(event) {
    angular.element(document.getElementById('exploreContent'))
      .removeAttr('style');
  });

  $scope.sendRequest = function() {
    RequestSrvc.add($scope.selectedTopics)
      .then(function(data) {
        if (data.id) {
          $ionicLoading.show({ template: 'Request sent', noBackdrop: true, duration: 2000 });
        } else {
          $ionicLoading.show({ template: 'No matched users', noBackdrop: true, duration: 2000 });
        }
      });
  };

  $scope.getTopics = getTopics;
});
