angular.module('app.explore.services', [])

.factory('TopicSrvc', function($http, BaseApiUrl) {
  return {
    get: function(prefix) {
      if (prefix) {
        return $http.get(BaseApiUrl + '/topic/' + prefix)
          .then(function(result) {
            return result.data;
          });
      } else {
        return $http.get(BaseApiUrl + '/topic')
          .then(function(result) {
            return result.data;
          });
      }

    },
    getMine: function() {
      return $http.get(BaseApiUrl + '/me/topic')
        .then(function(result) {
          return result.data ? result.data : [];
        });
    },
    add: function(topic) {
      return $http.put(BaseApiUrl + '/topic', {
        name: topic
      });
    },
    remove: function(topic) {
      return $http.delete(BaseApiUrl + '/topic/' + topic);
    }
  };
});
