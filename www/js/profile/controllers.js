angular.module('app.profile.controllers', ['satellizer'])

.controller('ProfileCtrl', function($scope, $ionicPopup, $auth, $ionicHistory,
    $state, UserSrvc, DefaultPicture, Upload, BaseApiUrl) {
  $scope.$on('$ionicView.enter', function() {
    UserSrvc.me()
      .then(function(user) {
        $scope.user = user;
      });
  });

  $scope.update = function() {
    UserSrvc.update($scope.user)
      .then(function(user) {
        if (user) {
          $scope.user = user;
        }
      })
      .catch(function(error) {
        $ionicPopup.alert({
          title: 'Error Occurred',
          template: error
        });
      });
  };

  $scope.logout = function() {
    $auth.logout().then(function() {
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        historyRoot: true
      });
      $state.go('auth.login');
    });
  };

  $scope.upload = function (files) {
    if (files && files.length) {
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        Upload.upload({
            url: BaseApiUrl + '/picture/upload',
            file: file
        }).success(function (data, status, headers, config) {
          console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
        });
      }
    }
  };
  $scope.defaultPicture = DefaultPicture;
})

.controller('ImageUploadCtrl', function($scope, ImageUploader) {
  $scope.$watch('files', function (files) {
    if (files && files.length) {
      ImageUploader.upload(files[0]);
    }
  });
})

.controller('PictureCtrl', function($scope, $stateParams, $ionicActionSheet,
    UserSrvc, DefaultPicture, ImageUploader) {
  function showEdit() {
    $ionicActionSheet.show({
     buttons: [
       { text: 'Take Photo' },
       { text: 'Choose Picture' }
     ],
     destructiveText: 'Delete Picture',
     cancelText: 'Cancel',
     buttonClicked: function(index) {
       navigator.camera.getPicture(function(fileUrl) {
         //  var image = 'data:image/jpeg;base64,' + imageData;
         window.resolveLocalFileSystemURL(fileUrl, function (fileEntry) {
           fileEntry.file(function (file) {
             ImageUploader.upload(file);
            //  var dataUrl = window.URL.createObjectURL(file);
           });
         });
        //  ImageUploader.upload(imageData);
       }, angular.noop, {
         destinationType: Camera.DestinationType.FILE_URI,
         sourceType: index === 0 ? Camera.PictureSourceType.CAMERA :
           Camera.PictureSourceType.PHOTOLIBRARY,
         saveToPhotoAlbum: false
       });
       return true;
     },
     destructiveButtonClicked: function() {
       UserSrvc.update({
         picture: null
       });
       return true;
     }
   });
  }

  $scope.$on('$ionicView.enter', function() {
    $scope.picture = $stateParams.url;
  });

  $scope.defaultPicture = DefaultPicture;
  $scope.edit = showEdit;

})

;
