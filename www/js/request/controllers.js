angular.module('app.request.controllers', [])

.controller('RequestsListCtrl', function($scope, $auth, $ionicPopup,
    RequestSrvc, UserSrvc, DefaultPicture) {
  var myId = UserSrvc.getId();
  var page = 0;

  function checkRequests() {
    if ($scope.requests.length === 0) {
      $scope.noRequests = true;
    } else {
      $scope.noRequests = false;
    }
  }

  function showConfirmationPopup(request, $index) {
    $ionicPopup.show({
      template: 'if you want to start chating',
      title: 'Confirm the request',
      scope: $scope,
      buttons: [
        {
          text: '<b>Confirm</b>',
          type: 'button-positive',
          onTap: function(e) {
            RequestSrvc.confirm(request.id);
            $scope.requests.splice($index, 1);
            checkRequests();
          }
        },
        {
          text: '<b>Decline</b>',
          type: 'button-assertive',
          onTap: function(e) {
            RequestSrvc.decline(request.id);
            $scope.requests.splice($index, 1);
            checkRequests();
          }
        }
      ]
    });
  }

  function showCancelationPopup(request, $index) {
    $ionicPopup.show({
      title: 'Revoke your request',
      scope: $scope,
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: '<b>Revoke</b>',
          type: 'button-assertive',
          onTap: function(e) {
            RequestSrvc.decline(request.id);
            $scope.requests.splice($index, 1);
            checkRequests();
          }
        }
      ]
    });
  }

  $scope.requests = [];
  $scope.defaultPicture = DefaultPicture;

  $scope.$on('$ionicView.enter', function() {
    page = 0;
    $scope.moreDataCanBeLoaded = true;
    $scope.loadMore();
  });

  $scope.$on('$stateChangeStart', function() {
    $scope.requests = [];
  });

  $scope.showPopup = function(request, $index) {
    if (request.senderId === myId) {
      showCancelationPopup(request, $index);
    } else {
      showConfirmationPopup(request, $index);
    }
  };

  $scope.loadMore = function() {
    RequestSrvc.get(myId, page).then(function(requests) {
      if (!requests || requests.length === 0) {
        $scope.moreDataCanBeLoaded = false;
      }
      $scope.requests = $scope.requests.concat(requests);
      checkRequests();
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
    page++;
  };
});
