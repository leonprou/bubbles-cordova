angular.module('app.request.services', [])

.factory('RequestSrvc', function($http, BaseApiUrl) {
  return {
    get: function(userId, page) {
      return $http.get(BaseApiUrl + '/request/' + userId + '/' + page)
        .then(function(result) {
          return result.data;
        });
    },
    add: function(topics) {
      return $http.post(BaseApiUrl + '/request', {
        topics: _.pluck(topics, 'name')
      })
        .then(function(result) {
          return result.data;
        });
    },
    confirm: function(id) {
      return $http.put(BaseApiUrl + '/request/' + id)
        .then(function(result) {
          return result.data;
        });
    },
    decline: function(id) {
      return $http.delete(BaseApiUrl + '/request/' + id)
        .then(function(result) {
          return result.data;
        });
    }
  };
});
